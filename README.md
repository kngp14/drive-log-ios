# ekoio Fahrtenbuch

[![ekoio Logo](https://ekoio-static.s3.amazonaws.com/static/img/ekoio_logo.png)](https://www.ekoio.com/de/)

### Zielstellung:
> Entwicklung einer iOS- und Android-App für die Firma ekoio UG für den mobilen Zugriff auf das Fahrtenbuch des ekoio-Webservices – bisher nur über Webportal möglich.

<img src="https://gitlab.com/kngp14/drive-log-android/raw/develop/resources/mockup/ios.png" alt="Mockup" width="700">

### App-Funktionen
* Klassifizierung einer Fahrt (Geschäftlich, Privat, ...)
* Angabe des Reisezwecks und ggf. getankter Liter
* Übermittlung der Daten an Webservice ([ekoio.com](https://www.ekoio.com/de/))
* Hintergrund: Abrechnung von Dienstfahrten o.ä.

### Technische Informationen
* ekoio-Stick liest Fahrzeugdaten über OBD-Schnittstelle aus und übermittelt Fahrtinformationen an Webservice
* Nach der Installation erfolgt einmalige Anmeldung am Webservice – Zugangsdaten werden gespeichert
* App ermöglicht Angabe zusätzlicher Informationen zu einer Fahrt durch Abruf und Anzeige bisheriger Fahrten (bisher nur über Weboberfläche nach der Fahrt möglich)
* Zeitpunkte zum Zuweisen der Informationen:
  * Vor Fahrtbeginn – Webservice weist diese der nächsten Fahrt zu
  * Nach der Fahrt – App weist Informationen einer Fahrt zu
* Bereitstellung der App-Funktionalität, kein professionelles GUI-Design

### Zeitvorgaben
Installation und Bereitstellung der App bei ekoio Januar/Februar 2017.

### Resourcen
* [Screenshots](https://gitlab.com/kngp14/drive-log-android/tree/develop/resources/mockup), Logos, Icons etc. sind im Ordner [resources](https://gitlab.com/kngp14/drive-log-android/tree/develop/resources) des Android-Repository zu finden.

### Wiki
Gehe zu: [GitLab Wiki im Android-Repository](https://gitlab.com/kngp14/drive-log-android/wikis/home)

### Entwickler
* [Tobias Donix](https://gitlab.com/tobiasd)
* [Kevin Klein](https://gitlab.com/zer0knowledge)
* [Kevin Neumann](https://gitlab.com/kngp14)
* [Maximilian Staab](https://gitlab.com/maxstaab)

### Lizenz
BSD 2.0 (siehe [LICENSE](https://gitlab.com/kngp14/drive-log-ios/blob/develop/LICENSE))


[![SPINLAB](https://ekoio-static.s3.amazonaws.com/static/img/SpinLab.png)](https://www.ekoio.com/de/)