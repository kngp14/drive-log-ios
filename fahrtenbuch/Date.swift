//
//  Date.swift
//  fahrtenbuch
//
//  BSD 2-Clause License
//
//  Copyright (c) 2016, Kevin Klein
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification,
//  are permitted provided that the following conditions are met:
//  1. Redistributions of source code must retain the above copyright notice, this list of
//  conditions and the following disclaimer.
//
//  2. Redistributions in binary form must reproduce the above copyright notice, this list
//  of conditions and the following disclaimer in the documentation and/or other
//  materials provided with the distribution.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
//  AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//  HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
//  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
//  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
//  POSSIBILITY OF SUCH DAMAGE.
//

import Foundation

class DateUtil {
    static func getStdDateFilter() -> [String] {
        var retVal: [String] = []
        let now = Date()
        let filterStartDate = Calendar.current.date(byAdding: .day, value: -3, to: now)!
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd'T'23:59:59'Z'"
        formatter.timeZone = TimeZone(identifier: "UTC")
        
        retVal.append(formatter.string(from: now))
        
        formatter.dateFormat = "yyyy-MM-dd'T'00:00:00'Z'"
        
        retVal.append(formatter.string(from: filterStartDate))
        
        return retVal
    }
}

class DateConverter {
    let date: Date
    
    init(str: String) {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        formatter.timeZone = TimeZone(identifier: "UTC")
        
        date = formatter.date(from: str)!
    }
    
    func toGermanStdDay() -> String {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "dd. MMMM yyyy"
        
        return formatter.string(from: date)
    }
    
    func toGermanStdTime() -> String {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "HH:mm"
        
        return formatter.string(from: date)
    }
}
