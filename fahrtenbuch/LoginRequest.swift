//
//  LoginRequest.swift
//  fahrtenbuch
//
//  BSD 2-Clause License
//
//  Copyright (c) 2016, Kevin Klein
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification,
//  are permitted provided that the following conditions are met:
//  1. Redistributions of source code must retain the above copyright notice, this list of
//  conditions and the following disclaimer.
//
//  2. Redistributions in binary form must reproduce the above copyright notice, this list
//  of conditions and the following disclaimer in the documentation and/or other
//  materials provided with the distribution.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
//  AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//  HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
//  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
//  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
//  POSSIBILITY OF SUCH DAMAGE.
//

class LoginRequest {

    private var cookie: String?
    private var httpCode: Int?
    private var usrMsg: String?
    private var success: Bool = false
    private var cookieVals: [String: String]?
    
    func loginByEmail(email: String, password: String, callback: @escaping (LoginRequest) -> Void) {
        execLoginReq(data: ["email": email, "password": password], callback: callback)
    }
    
    func loginByUsername(username: String, password: String, callback: @escaping (LoginRequest) -> Void) {
        execLoginReq(data: ["username": username, "password": password], callback: callback)
    }
    
    func logout(callback: @escaping (LoginRequest) -> Void) {
        ekoioApi.logout.request(.post, json: [:])
            
            .onSuccess() { _ in
                self.success = true
                
                debugPrint("[ekoio] Logout erfolgreich")
                
                callback(self)
            }
            
            .onFailure() { error in
                self.httpCode = error.httpStatusCode
                self.success = false
                self.usrMsg = "Die Anfrage ist fehlgeschlagen. Bitte versuchen Sie es später erneut."
                
                debugPrint("[ekoio] Logout fehlgeschlagen")
                debugPrint(self.usrMsg!)
                debugPrint(self.httpCode!)
                debugPrint(error.userMessage)
                
                callback(self)
        }
    }
    
    private func execLoginReq(data: [String: String], callback: @escaping (LoginRequest) -> Void) {
        ekoioApi.login.request(.post, json: data)
            
            .onSuccess() {
                self.cookie = $0.header(forKey: "Set-Cookie")
                self.cookieVals = self.splitCookie(cookie: self.cookie!) //Split by ;
                self.success = true
                
                debugPrint("[ekoio] Login erfolgreich")
                debugPrint(self.cookie!)
                
                callback(self)
            }
            
            .onFailure() { error in
                self.httpCode = error.httpStatusCode
                self.success = false
                
                if(self.httpCode == 400) {
                    self.usrMsg = "Die angegebenen Zugangsdaten waren leider nicht korrekt"
                } else {
                    self.usrMsg = "Die Anfrage ist fehlgeschlagen. Bitte versuchen Sie es später erneut."
                }
                
                debugPrint("[ekoio] Login fehlgeschlagen")
                debugPrint(self.usrMsg!)
                debugPrint(self.httpCode!)
                debugPrint(error.userMessage)
                
                callback(self)
            }
    }
    
    private func splitCookie(cookie: String) -> [String: String] {
        let pairs: [String] = cookie.components(separatedBy: ";")
        var retVal: [String: String] = [:]
        
        for pair: String in pairs {
            let keyAndVal: [String] = pair.splitByFirst(chr: "=")
            
            if (keyAndVal.count != 2) { continue }
            
            let key: String = keyAndVal[0].trim()
            let val: String = keyAndVal[1].trim()
            
            //Don't override
            if(retVal[key] != nil) {
                continue
            }
            
            if(key == "Path") {
                //Path contains something like Path=/, sessionid=... and needs to be splitted again
                // first by comma and second again by equal sign
                let valSplitted = val.splitByFirst(chr: ",")
                
                retVal[key] = valSplitted[0].trim()
                
                //Second split by equal sign
                let valSecSplitted = valSplitted[1].splitByFirst(chr: "=")
                let vSSKey = valSecSplitted[0].trim()
                let VSSBVal = valSecSplitted[1].trim()
                
                retVal[vSSKey] = VSSBVal
            } else {
                retVal[key] = val
            }
        }
        
        return retVal
    }
    
    func wasSuccessfull() -> Bool {
        
        return success
    }
    
    func getErrorMsg() -> String? {
        
        return usrMsg
    }
    
    func getExpDate() -> String? {
        return cookieVals?["expires"]
    }
    
    func getCSRF() -> String? {
        if let csrf = cookieVals?["csrftoken"] {
            return csrf;
        }
        
        return nil
    }
    
    func getAuthString() -> String? {
        if let sID = cookieVals?["sessionid"], let csrf = cookieVals?["csrftoken"] {
            return "csrftoken=" + csrf + ";sessionid=" + sID + ";";
        }
        
        return nil
    }
}
