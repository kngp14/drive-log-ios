//
//  TTViewController.swift
//  fahrtenbuch
//
//  BSD 2-Clause License
//
//  Copyright (c) 2016, Kevin Klein
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification,
//  are permitted provided that the following conditions are met:
//  1. Redistributions of source code must retain the above copyright notice, this list of
//  conditions and the following disclaimer.
//
//  2. Redistributions in binary form must reproduce the above copyright notice, this list
//  of conditions and the following disclaimer in the documentation and/or other
//  materials provided with the distribution.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
//  AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//  HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
//  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
//  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
//  POSSIBILITY OF SUCH DAMAGE.
//

import UIKit
import Siesta

class TTViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, ResourceObserver {
    
    @IBOutlet var tableView: UITableView!
    var stripsRes: Resource!
    var statusOverlay = ResourceStatusOverlay()
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var trips: [Trip] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        stripsRes.loadIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(TTViewController.refreshResource), name: .UIApplicationDidBecomeActive, object: nil)
        
        setResource()
        
        statusOverlay.displayPriority = [.loading]
        statusOverlay.containerView?.backgroundColor = UIColor.white
        statusOverlay.embed(in: self)
        
        stripsRes
            .addObserver(self)
            .addObserver(statusOverlay)
        
        ekoioApi.strips.addObserver(owner: self) {
            [weak self] resource, event in
            self!.stripsRes.invalidate()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setResource()
        stripsRes.loadIfNeeded()
    }
    
    func setResource() {
        let stdFilter = DateUtil.getStdDateFilter()
        stripsRes = ekoioApi.strips
            .withParam("first_datetime_1", stdFilter[0])
            .withParam("first_datetime_0", stdFilter[1])
        
        tableView.setContentOffset(CGPoint.zero, animated: false)
    }
    
    func refreshResource() {
        setResource()
        
        stripsRes.load()
    }
    
    func resourceChanged(_ resource: Resource, event: ResourceEvent) {
        trips = stripsRes.typedContent() ?? []
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return trips.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let trip = trips[(indexPath as IndexPath).row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TripCell
        let startDC:DateConverter = DateConverter(str: trip.startDate)
        let endDC:DateConverter = DateConverter(str: trip.endDate)
        
        cell.date.text = startDC.toGermanStdDay() + " — " + trip.distance + " km"
        cell.start.text = trip.start + ", " + startDC.toGermanStdTime() + " Uhr"
        cell.end.text = trip.end + ", " + endDC.toGermanStdTime() + " Uhr"
        
        if(trip.type == TripType.none) {
            cell.icon.image = UIImage(named: "icon_arrow_gray")
        } else {
            cell.icon.image = UIImage(named: "icon_arrow_green")
        }
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "logout") {
            let loginViewController = segue.destination as! LoginViewController
        
            LoginSettings.setHasLoggedIn(hasLoggedIn: false)
            loginViewController.doLogoutFirst = true
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let trip = trips[(indexPath as IndexPath).row]
        var alertMsg: String
        var optionStyles: [UIAlertActionStyle] = [.default, .default]
        var method = "patch"
        
        if(trip.type == TripType.none) {
            alertMsg = "Diese Fahrt wurde noch nicht zugeordnet. Wählen Sie eine Klassifizierung."
            method = "post"
        } else {
            alertMsg = "Diese Fahrt wurde bereits als "
            
            switch trip.type {
            case TripType.businessTrip:
                alertMsg += "geschäftlich"
                optionStyles[1] = .destructive
            case TripType.privateTrip:
                alertMsg += "privat"
                optionStyles[0] = .destructive
            default: break
            }
            
            alertMsg += " markiert. Wählen Sie eine neue Klassifizierung."
        }
        
        let alertController = UIAlertController(title: nil, message: alertMsg, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Abbrechen", style: .cancel) { (action) in
            tableView.deselectRow(at: indexPath, animated: true)
        }
        
        
        let markBusinessAction = UIAlertAction(title: "Geschäftlich", style: optionStyles[0]) { (action) in
            TripAssignment.assignTrip(id: trip.id, type: 0, method: method, callback: self.assignmentResponseHandler)
        }
        
        let markPrivateAction = UIAlertAction(title: "Privat", style: optionStyles[1]) { (action) in
            TripAssignment.assignTrip(id: trip.id, type: 1, method: method, callback: self.assignmentResponseHandler)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(markBusinessAction)
        alertController.addAction(markPrivateAction)
        
        self.present(alertController, animated: true)
    }
    
    func assignmentResponseHandler(success: Bool) {
        if(success) {
            refreshResource()
        } else {
            let alert = UIAlertController(title: "Fehler", message: "Die Datenübermittlung ist leider fehlgeschlagen. Bitte versuchen Sie es später erneut.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
    }

}
