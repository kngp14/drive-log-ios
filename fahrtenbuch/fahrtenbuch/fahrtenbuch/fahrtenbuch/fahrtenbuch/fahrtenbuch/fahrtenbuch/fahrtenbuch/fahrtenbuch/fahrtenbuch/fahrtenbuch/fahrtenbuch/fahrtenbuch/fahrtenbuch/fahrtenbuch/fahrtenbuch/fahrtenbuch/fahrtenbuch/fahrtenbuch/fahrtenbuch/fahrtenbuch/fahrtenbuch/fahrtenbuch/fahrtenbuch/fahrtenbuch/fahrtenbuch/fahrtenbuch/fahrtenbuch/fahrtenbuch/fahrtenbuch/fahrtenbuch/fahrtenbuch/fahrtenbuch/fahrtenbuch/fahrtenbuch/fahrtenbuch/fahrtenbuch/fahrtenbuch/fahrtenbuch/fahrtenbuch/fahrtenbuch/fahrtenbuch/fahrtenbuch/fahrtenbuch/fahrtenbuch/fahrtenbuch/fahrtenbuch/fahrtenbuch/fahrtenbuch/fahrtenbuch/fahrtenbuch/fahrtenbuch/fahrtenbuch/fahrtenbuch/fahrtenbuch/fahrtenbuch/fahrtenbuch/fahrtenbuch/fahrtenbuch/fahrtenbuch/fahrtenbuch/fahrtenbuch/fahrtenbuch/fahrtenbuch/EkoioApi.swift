//
//  EkoioApi.swift
//  fahrtenbuch
//
//  BSD 2-Clause License
//
//  Copyright (c) 16.11.2016, Kevin Klein
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification,
//  are permitted provided that the following conditions are met:
//  1. Redistributions of source code must retain the above copyright notice, this list of
//  conditions and the following disclaimer.
//
//  2. Redistributions in binary form must reproduce the above copyright notice, this list
//  of conditions and the following disclaimer in the documentation and/or other
//  materials provided with the distribution.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
//  AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//  HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
//  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
//  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
//  POSSIBILITY OF SUCH DAMAGE.
//

import Siesta
import SwiftyJSON

class EkoioApi: Service {
    init() {
        #if DEBUG
        super.init(baseURL: "https://stage.ekoio.com")
        #else
        super.init(baseURL: "https://www.ekoio.com")
        #endif
        
        configure(whenURLMatches: { $0.path != "/rest-auth/login/" }) {
            $0.headers["Cookie"] = self.authHeader
            $0.headers["X-CSRFToken"] = self.csrfToken
            $0.headers["Referer"] = self.baseURL?.absoluteString
            $0.pipeline[.parsing].add(SwiftyJSONTransformer, contentTypes: ["*/json"])
        }
        
        //Transformer
        configureTransformer("/api/v1/drivedata/small-trips/") {
            try ($0.content as JSON)["results"].arrayValue.map(Trip.init)
        }
    }
    
    var authHeader: String? {
        didSet
        {
            // Clear any cached data now that auth has changed
            wipeResources()
            
            // Force resources to recompute headers next time they’re fetched
            invalidateConfiguration()
        }
    }
    
    var csrfToken: String? {
        didSet
        {
            // Clear any cached data now that auth has changed
            wipeResources()
            
            // Force resources to recompute headers next time they’re fetched
            invalidateConfiguration()
        }
    }
    
    //User Management
    var login: Resource { return resource("/de/rest-auth/login/") }
    var logout: Resource { return resource("/de/rest-auth/logout/") }
    
    //Trips
    var strips: Resource { return resource("/api/v1/drivedata/small-trips/") }
}

let ekoioApi = EkoioApi()
