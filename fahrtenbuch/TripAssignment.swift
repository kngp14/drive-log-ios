//
//  TripAssignment.swift
//  fahrtenbuch
//
//  BSD 2-Clause License
//
//  Copyright (c) 2016, Kevin Klein
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification,
//  are permitted provided that the following conditions are met:
//  1. Redistributions of source code must retain the above copyright notice, this list of
//  conditions and the following disclaimer.
//
//  2. Redistributions in binary form must reproduce the above copyright notice, this list
//  of conditions and the following disclaimer in the documentation and/or other
//  materials provided with the distribution.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
//  AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//  HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
//  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
//  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
//  POSSIBILITY OF SUCH DAMAGE.
//

import SwiftyJSON
import Siesta

class TripAssignment {
    static func assignTrip(id: Int, type: Int, method: String, callback: @escaping (Bool) -> Void) {
        let res = ekoioApi.tripData.withParam("trip_pk", String(id))
        
        
        res.request(.get)
            .onSuccess(){data in
                sendAssignmentReq(json: data.jsonDict as! [String : JSON], type: type, method: method, callback: callback)
            }
    }
    
    static private func updateAdressFormat(address: String) -> [String: String]{
        let splittedVals = address.splitByFirst(chr: ",")
        return ["street": splittedVals[0], "city": splittedVals[1].splitByFirst(chr: ",")[0]]
    }
    
    static private func sendAssignmentReq(json: [String: JSON], type: Int, method: String, callback: @escaping (Bool) -> Void) {
        //In Dict mit primitiven DT kopieren
        var dict: [String: Any] = [:]
        
        for (key, val) in json {
            switch key {
            case "start_datetime":
                dict[key] = val.string!
            case "end_datetime":
                dict[key] = val.string!
            case "start_address":
                dict[key] = updateAdressFormat(address: val.string!)
            case "end_address":
                dict[key] = updateAdressFormat(address: val.string!)
            case "trip":
                dict[key] = val.int!
            case "drive_log_pk": break //nothing
            case "start_mileage":
                dict[key] = val.int!
            case "end_mileage":
                dict[key] = val.int!
            case "distance":
                dict[key] = val.int!
            default:
                dict[key] = val.string!
            }
        }
        
        dict["type"] = type
        
        if(method == "patch") {
            let id = json["drive_log_pk"]!.int!
            
            ekoioApi.drivelogs.child(String(id) + "/").request(.patch, json: dict)
                .onSuccess() {_ in
                    callback(true)
                }
                .onFailure {error in
                    callback(false)
            }
        } else {
            ekoioApi.drivelogs.request(.post, json: dict)
                .onSuccess() {_ in
                    callback(true)
                }
                .onFailure {error in
                    callback(false)
            }
        }
    }
}
