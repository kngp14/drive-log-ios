//
//  LoginViewController.swift
//  fahrtenbuch
//
//  BSD 2-Clause License
//
//  Copyright (c) 2016, Kevin Klein
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification,
//  are permitted provided that the following conditions are met:
//  1. Redistributions of source code must retain the above copyright notice, this list of
//  conditions and the following disclaimer.
//
//  2. Redistributions in binary form must reproduce the above copyright notice, this list
//  of conditions and the following disclaimer in the documentation and/or other
//  materials provided with the distribution.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
//  AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//  HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
//  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
//  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
//  POSSIBILITY OF SUCH DAMAGE.
//

import Foundation
import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet var loginName: UITextField!
    @IBOutlet var loginPassword: UITextField!
    @IBOutlet var loginBtn: UIButton!
    
    var doLogoutFirst = false
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func loginBtnPressed(_ sender: Any) {
        //deactivate
        showWaitDlg()
        
        let loginReq = LoginRequest()
        
        if(loginName.text!.isValidEmail()) {
            loginReq.loginByEmail(email: loginName.text!, password: loginPassword.text!, callback: loginResponseHandler)
        } else {
            loginReq.loginByUsername(username: loginName.text!, password: loginPassword.text!, callback: loginResponseHandler)
        }
    }
    
    override func viewDidLoad() {
        if(doLogoutFirst) {
            let loginReq = LoginRequest()
            
            loginReq.logout(callback: logoutCallback)
        }
        
        setBorderStyle(field: loginName)
        setBorderStyle(field: loginPassword)
        
        loginName.autocorrectionType = .no
        loginPassword.autocorrectionType = .no
        
        loginName.becomeFirstResponder()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //Wenn Logout noch nicht abgeschlossen, zeige Wartendialog
        //Logout-Callback setzt Variable auf false
        if(doLogoutFirst) {
            showWaitDlg()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        // Try to find next responder
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        // Do not add a line break
        return false
    }
    
    func setBorderStyle(field: UITextField) {
        //Calc width
        let width = UIScreen.main.bounds.width - 100 // Textfield - 50 left - 50 right
        
        //Bottom border
        let border = CALayer()
        border.frame = CGRect(x: 0, y: field.frame.size.height - 1, width: width, height: 1);
        border.backgroundColor = field.tintColor.cgColor
        
        field.layer.addSublayer(border)
        field.layer.masksToBounds = true
    }
    
    func showWaitDlg() {
        
    }
    
    func loginResponseHandler(loginReq: LoginRequest) {
        
        if (loginReq.wasSuccessfull()) {
            ekoioApi.authHeader = loginReq.getAuthString()
            ekoioApi.csrfToken = loginReq.getCSRF()
            
            LoginSettings.setAuthHeader(str: loginReq.getAuthString()!)
            LoginSettings.setCSRFToken(str: loginReq.getCSRF()!)
            LoginSettings.setHasLoggedIn(hasLoggedIn: true)
            
            self.performSegue(withIdentifier: "showTrips", sender: self)
        } else {
            let alert = UIAlertController(title: "Fehler", message: loginReq.getErrorMsg(), preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func logoutCallback(loginReq: LoginRequest) {
        doLogoutFirst = false //Kontrollvariable zurücksetzen
    }
}
