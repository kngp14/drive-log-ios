//
//  Trip.swift
//  fahrtenbuch
//
//  BSD 2-Clause License
//
//  Copyright (c) 2016, Kevin Klein
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification,
//  are permitted provided that the following conditions are met:
//  1. Redistributions of source code must retain the above copyright notice, this list of
//  conditions and the following disclaimer.
//
//  2. Redistributions in binary form must reproduce the above copyright notice, this list
//  of conditions and the following disclaimer in the documentation and/or other
//  materials provided with the distribution.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
//  AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//  HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
//  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
//  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
//  POSSIBILITY OF SUCH DAMAGE.
//

import SwiftyJSON

enum TripType {
    case businessTrip
    case privateTrip
    case none
}

struct Trip {
    let id: Int
    let startDate: String
    let endDate: String
    let start: String
    let end: String
    let distance: String
    let type: TripType
    
    init(json: JSON) throws {
        id = json["id"].intValue
        startDate = json["first_datetime"].stringValue
        endDate = json["last_datetime"].stringValue
        start = json["start_address"]["city"].stringValue.splitByFirst(chr: " ", pos: 1) //Only City
        end = json["end_address"]["city"].stringValue.splitByFirst(chr: " ", pos: 1) //Only City
        
        //Meter to Kilometer 
        //Round to one decimal place
        distance = String(format: "%.1f", (json["distance"].doubleValue / 1000).roundTo(places: 1))
        
        if(json["drive_log"] == JSON.null) {
            type = TripType.none
        } else {
            switch json["drive_log"]["type"].intValue {
            case 0:
                type = TripType.businessTrip
            case 1:
                type = TripType.privateTrip
            default:
                type = TripType.businessTrip
            }
        }
        
    }
}
