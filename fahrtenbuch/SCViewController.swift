//
//  SCViewController.swift
//  fahrtenbuch
//
//  BSD 2-Clause License
//
//  Copyright (c) 2016, Kevin Klein
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification,
//  are permitted provided that the following conditions are met:
//  1. Redistributions of source code must retain the above copyright notice, this list of
//  conditions and the following disclaimer.
//
//  2. Redistributions in binary form must reproduce the above copyright notice, this list
//  of conditions and the following disclaimer in the documentation and/or other
//  materials provided with the distribution.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
//  AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//  HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
//  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
//  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
//  POSSIBILITY OF SUCH DAMAGE.
//

import UIKit
import SwiftGifOrigin

class SCViewController: UIViewController {

    //Image
    let imgFileName:String = "ekoio_linear"
    
    //ekoio Image
    @IBOutlet var SPScreenImage: UIImageView!
    
    //Hide Statusbar in Splash Screen
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    //View appeared
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let loaderGif = UIImage.gif(name: imgFileName)
        let loaderDuration = 4;
        
        SPScreenImage.image = loaderGif
        
        //Wait until animation finishes
        _ = Timer.scheduledTimer(timeInterval: TimeInterval(loaderDuration), target: self, selector: #selector(self.switchToLogin), userInfo: nil, repeats: false);
    }
    
    //Switch to Login View
    func switchToLogin() {
        if(LoginSettings.hasLoggedIn()) {
            ekoioApi.authHeader = LoginSettings.getAuthHeader()
            ekoioApi.csrfToken = LoginSettings.getCSRFToken()
            
            self.performSegue(withIdentifier: "showTrips", sender: self)
        } else {
            self.performSegue(withIdentifier: "showLogin", sender: self)
        }
    }
}

